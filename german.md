# Eine Weihnachtsgeschichte

<pre>
C.Z.42b.2", "C.Z.42b.2" flüstert Bob vor sich hin, während er hektisch durch
die Gänge läuft, links und rechts an die Schilder der Räume schaut und den
richtigen Raum sucht. Aber um den Weg fragen darf er niemanden. Das Christkind
hat um äußerste Diskretion gebeten, oberste Geheimhaltungsstufe! Das ist
vermutlich auch der Grund warum das Treffen in der hintersten Ecke des Gebäudes
stattfinden muss.

"C.Z.101010b.10", "ah, ENDLICH gefunden!". Noch ein schneller Blick auf die Uhr:
12:00:03. "Verdammt schon wieder 3 Sekunden zu spät, hoffentlich wird das
Christkind nicht sauer.", denkt sich Bob als er kurz tief einatmet, den Raum
betritt und die Tür schnell wieder schließt, um ja nicht bemerkt zu werden.

"Ah, Bob da bist du ja. Nun ihr fragt euch sicher alle warum ich euch so
kurzfristig hierher gebeten habe ..", hört er das Christkind sagen. "Alle?"
denkt sich Bob und schaut sich um. Bei den Tischen im Raum sieht er Alice, Dave
und Carrol sitzen. "Das bedeutet nichts Gutes", denkt Bob weiter, "dass das
Christkind dringende Hilfe von mir, dem besten Reverse-Engineer unter
den Engeln, braucht, mag ja schon Grund zur Besorgnis geben. Aber, dass er
auch noch Dave, den Cyberdefense-Spezialisten, Alice, die beste Forensikerin
wenn es um digitale Spurensuche geht, und Carrol, die leitende
Krisenmanagerin, gleichzeitig zu sich ruft bedeutet gar nichts Gutes.".

".. mir ist ein großes Missgeschick passiert.", setzt das Christkind fort. "Wie
ihr alle wisst müssen wir morgen mit der Auslieferung der Weihnachtsgeschenke
beginnen. Doch leider haben wir keinen Zugriff mehr auf die Datenbank mit den
Listen der braven und nicht-so-braven Kinder." Dave erinnert sich, dass sich die
Engel letztes Jahr stark gemacht haben, dass die Listen endlich digitalisiert
werden, um den Aufwand zu verringern. Er selbst hat den Großteil des Systems
implementiert, damit es auch garantiert sicher ist. Doch wieso hat das
Christkind keinen Zugriff mehr darauf? Was ist passiert? "Ich habe das
root-Passwort vergessen." beantwortet das Christkind die frage, ohne dass Dave
sie ausgesprochen hätte.

Allgemeine Stille. Die Engel werfen sich untereinander ratlose Blicke zu.
"Daher brauche ich eure Hilfe das Passwort wieder her zu stellen." "Das wird
nicht gehen", entgegnet Dave, "das System ist 100% sicher.". "100% sicher ist
NICHTS" meint Carrol, "Wir müssen es versuchen. Sonst wird das Weihnachtsfest
eine katastrophe!".

"Also gut", beginnt Dave während er seinen Laptop aufklappt und die Kali-VM
startet. Das Christkind wollte damals unbedingt ein Bootstrap-basiertes
RESTful Web-Interface haben, damit es mit verschiedensten Browsern und
User-Agents darauf zugreifen kann. Da das Christkind das Passwort ja vergessen
hat und somit auch keine gültige session mit zugehörigem Session-Cookie mehr
hat, fällt die Möglichkeit einer Session-Hijacking-Attacke mittels XSS aus.
Nachdem Kali fertig gebootet hat und während Dave darauf wartet, dass die
Burpsuite fertig geladen hat, checkt er sein lokales git-Repository und öffnet
den Source Code des Backends.

Während sich Dave eifrig durch seinen alten Code liest hat auch Bob bereits
seinen Laptop gestartet.Das Christkind greift zwar über das moderne
Web-Interface auf die Datenbank zu, aber die Engel, welche Eintragungen über
brave und weniger brave Kinder machen, verwenden immer noch das alte System,
welches nur über SSH zugänglich ist. Auch Bob hat noch immer einen Account,
obwohl er schon seit einigen Jahren kein sogenannter Reporter mehr ist. "Ob
ich mich mal beim Management melden soll und sie darauf aufmerksam machen soll
die Access-Control-Listen zu aktualisieren?" überlegt er, während er in seinem
Passwort-manager nach den richtigen Credentials sucht. Nur zwei Sekunden
später ist er erfolgreich authentifiziert und ist mit einer shell auf dem
Backend verbunden. Leider sind seine User-Permissions sehr eingeschränkt,
weswegen er ohne weiterer Tricks nicht viel machen kann.

Glücklicherweise muss Bob nicht lange überlegen, um zu Wissen was er versuchen
kann. Mit einem einfachen command checkt er die Kernelversion des laufenden
Systems. Linux 4.8.14-200.fc24.x86_64 "Mist!", murmelt Bob vor sich hin
"leider zu aktuell, dafür gibt es noch keine bekannten CVEs die mir eine
privilage escalation erlauben". Zumindest weiß er, dass die Anwendung zum
Eintragen von Daten im Kontext des root-users ausgeführt wird, weil das setuid
Bit in den Dateiberechtigungen gesetzt ist. Er müsste also nur eine
Sicherheitslücke finden die ihm eine arbitrary code execution oder eine
command execution erlaubt. Dafür versucht er gdb, einen Debugger, zu starten.
Leider setzt die Anwendung Anti-Debugging-Techniken ein, weswegen der Debugger
sofort abstürzt. Da in diesem Fall, aufgrund des Zeitmangels, der Einsatz
eines Disassemblers nur wenig Sinn machen würde, beschließt Bob einen Fuzzer
zu starten, um einen segfault zu provozieren.

"Segmentation Fault (core dumped)". Glücklicherweise dauerte es nur wenige
Minuten bis der Fuzzer erfolgreich war. Aufgrund des Stacktraces kann Bob
erkennen, dass es in der Anwendung tatsächlich eine Buffer-Overflow
Vulnerability gibt. Jetzt müssen nur noch zwei Hürden überwunden werden: Stack
Canaries und ASLR.
Deswegen entscheidet sich Bob dazu, zusätzlich zu dem shellcode, noch eine
2048 Byte lange NOP-Sledge in den payload zu stecken. Um allerdings trotz ASLR
den instruction pointer mit der richtigen Sprungadresse zu überschreiben,
schreibt er sich schnell ein Bash-Script, welches mögliche Speicheradressen
brute forced.

Minuten der Anspannung vergehen. Dann hört man plötzlich von Dave und Bob
beinahe gleichzeitig ein lautes "Juhuuu!!". Sofort berichtet Dave: "Ich habe
eine Shell! Es war auch richtig einfach: Beim Durchlesen des Source Codes ist
mir aufgefallen, dass man wegen einer Sicherheitsabfrage die nur in der
lokalen Javascript-Engine, aber nicht am Server, ausgeführt wird eine file
inclusion durchführen kann. Das hat mir eine sql-injection ermöglicht und dank
einer directory traversal Schwachstelle in Kombination mit einem Fehler im
URL-Parser ist es mir gelungen eine reverse shell zu meinem System her zu
stellen! Jetzt können wir /etc/shadow überschreiben und den Login wieder
herstellen!"

Doch zu früh gefreut: Ernüchternd müssen beide feststellen, dass sie zwar
funktionierende shells haben, aber keine davon als root-user ausgeführt wird.
Ein allgemeines Seufzen geht durch den Raum.

"Dann lasst es mich versuchen!" ruft Alice, die bis jetzt den anderen beiden
zugeschaut hat. "Christkind, wo befindet sich die Maschine des Servers?" "Im
Raum Q.-2.17x.5, also am anderen Ende des Gebäudes" antwortet dieses, bereits
leicht verzweifelt. "Dann los! Ich kann noch eine forensische Analyse
versuchen!" ruft Alice während sie sich in Bewegung setzt. Nur 3 Minuten, 14
Sekunden später erreichen das Christkind und die Engel den Serverraum.
Entsetzt stellt Alice fest, dass die Maschine keinen Firewire-Anschluss hat
und auch keinen anderen Anschluss, welcher Direct Memory Access (DMA) erlauben
würde. Da die Festplatte des Servers allerdings auch verschlüselt ist brauchen
die Engel einen RAM-dump, um noch auf die User-Passwörter zugreifen zu können.
Daher weiß Alice, dass sie nur noch eine Cold-Boot-Attacke versuchen kann.
Dazu öffnet sie ihren Forensikkoffer, nimmt den Schraubenzieher und öffnet das
Gehäuse des Servers. Anschließend nimmt sie den Kältespray aus ihrem Koffer
und sagt zu Bob: "Ich zähle bis 3. Und bei 3 musst du den Stromstecker ziehen!
Danach muss alles ganz schnell gehen!" " .. 1, 2, 3!" Danach ging alles rasend
schnell: Strom ab, RAM-Riegel eingefroren, aus dem Server entfernt, in Alice'
Laptop rein, den gebootet und nur wenige Minuten später hatten sie einen
vollständigen dump vom RAM. Gerade als Alice volatility startet, um nach dem
Schlüssel des Festplattenpasswortes zu scannen hören die 4 Carrols Stimme vom
Gang rufen. Verwirrt blicken sich das Christkind, Alice, Bob und Dave an und
stellen erschrocken fest, dass Carrol gar nicht mehr da ist. Als diese bei der
Tür hereinstürzt meint sie keuchend "Ach, hier seid ihr! *schnauf* Warum
versteckt ihr euch denn? *schnauf* Ich habe schon überall nach euch
gesucht!" Als Carrol die Blicke der anderen sieht setzt sie fort "Sagt ja
nicht ihr hättet gar nicht bemerkt, dass ich gegangen bin!?" Als Antwort bekam
sie nur verlegene Blicke. "Na, egal. Wie auch immer. Ich habe eine gute und
eine schlechte Nachricht: Zuerst die schlechte: Christkind, du solltest eine
bessere Ordnung in deinem Büro halten! Ich habe ewig gebraucht, um da auf zu
räumen! Außerdem muss ich mit dir schimpfen! Ich habe dir schon tausend Mal
gesagt, dass du einen Passwort Manager verwenden sollst! Erstens hättest du
das Datenbank-Passwort dann gar nicht vergessen können und zweitens wäre es
dann niemanden möglich diesen Zettel hier zu finden." erklärte Carrol während
sie ein kleines Post-it nach vorne streckt. 

"Benutzer: Christkindl
Passwort: Fr0h3 W31hn4ch73n!"

ist darauf zu lesen.
Als Antwort bekam sie nur ein einstimmiges "Oh. Gott sei Dank!". Mit einem
Blick auf die Uhr, welche 12:11:16 anzeigt (Engel arbeiten schnell!),
vergewissert sich das Christkind, dass noch genug Zeit bleibt, um alle
Geschenke rechtzeitig zu verteilen. Doch bevor es dazu kommen kann druckt das
Christkind die Liste der braven Kinder aus (die Liste der
nicht-so-braven-Kinder ist leer, weder Bob noch Dave können sich erklären wie
das passieren könnte), ändert sein Passwort auf ein sicheres & zufälliges, und
speichert dieses in einem Passwortmanager.

Das Weihnachtsfest ist gerettet und ich wünsche euch und euren Familien &
Freunden frohe Weihnachten und angenehme Feiertage! (Und wer den einen oder
anderen Begriff nicht verstanden hat kann mich ja gerne Fragen ;)
</pre>

# Fachbegriffe
Und hier noch eine Liste alle vorgekommenen Fachbegriffe, leider hatte ich keine
Zeit mehr diese auch noch zu erklären:

* Reverse Engineer
* Forensikerin
* Datenbank
* root
* Kali
* VM
* Bootstrap
* RESTful
* Browser
* User-Agent
* Session-Hijacking
* XSS
* booten
* Burpsuite
* git-repository
* Source Code
* SSH
* Access Control List
* Passwortmanager
* Credentials
* Backend
* shell
* Kernel
* CVE
* privilage escalation
* setuid Bit
* arbitrary code execution
* command execution
* gdb
* Debugger
* Anti-Debugging-Techniken
* Disassembler
* Fuzzer
* segfault
* "Segmentation Fault (core dumped)"
* stacktrace
* Buffer Overflow
* Stack canary
* ASLR
* shellcode
* NOP-Sledge
* instruction pointer
* Sprungadresse
* Bash-Script
* Speicheradresse
* brute force
* Javascript
* Javascript-Engine
* file inclusion
* sql-injection
* directory traversal
* URL-Parser
* /etc/shadow
* Forensische Analyse
* Firewire
* Direct Memory Access (DMA)
* RAM
* RAM-Dump
* Cold-Boot-Attack
* RAM-Riegel
* Volatility